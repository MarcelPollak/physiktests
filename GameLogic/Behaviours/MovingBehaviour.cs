﻿using GameLogic.Behaviours.Interfaces;
using GameLogic.Entities.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic.Behaviours
{
    public class MovingBehaviour : IBehaviour
    {
        public void UpdateEntity(IHasImpulse entity)
        {
            entity.Position = new Vector2(entity.Position.X + entity.Speed.X, entity.Position.Y + entity.Speed.Y);

        }
    }
}
