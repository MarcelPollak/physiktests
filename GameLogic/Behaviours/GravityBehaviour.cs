﻿using GameLogic.Behaviours.Interfaces;
using GameLogic.Entities.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic.Behaviours
{
    public class GravityBehaviour : IBehaviour
    {
        private float gravity = 0.1f;

        public void UpdateEntity(IHasImpulse entity)
        {
            var newY = entity.Speed.Y + gravity;
            entity.Speed = new Vector2(entity.Speed.X, newY);
        }
    }
}
