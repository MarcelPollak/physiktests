﻿using GameLogic.Behaviours.Interfaces;
using GameLogic.Entities.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic.Entities
{
    public class Sphere : IHasImpulse
    {
        public Vector2 Position { get; set; }
        public Vector2 Speed { get; set; } = new Vector2(0, 0);
        public float Mass { get ; set; }

        private List<IBehaviour> _behaviours = new List<IBehaviour>();

        public Sphere(Vector2 position, Vector2 speed, float mass, List<IBehaviour> behaviours)
        {
            Position = position;
            Speed = speed;
            Mass = mass;
            _behaviours = behaviours;
        }

        public void Update()
        {
            foreach (var behaviour in _behaviours)
            {
                behaviour.UpdateEntity(this);
            }
        }

    }
}
