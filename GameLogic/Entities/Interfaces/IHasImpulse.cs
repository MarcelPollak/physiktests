﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic.Entities.Interfaces
{
    public interface IHasImpulse
    {
        Vector2 Speed { get; set; } 
        Vector2 Position { get; set; } 
        float Mass { get; set; } 
    }
}
