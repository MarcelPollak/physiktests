﻿using GameLogic.Behaviours;
using GameLogic.Behaviours.Interfaces;
using GameLogic.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic.Factories
{
    public static class StandartSphereFactory
    {
        public static Sphere CreateSphere(Vector2 position)
        {
            var behaviours = new List<IBehaviour>()
            {
                new GravityBehaviour(),
                new MovingBehaviour()
            };

            return new Sphere(position, new Vector2(0, 0), 10, behaviours);
        }
    }
}
