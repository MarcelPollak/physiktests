﻿using GameLogic.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysikTests.Drawer
{
    class SphereDrawer
    {
        private Texture2D _sphereTexture;
        private int _sphereSize = 20;

        public SphereDrawer(Texture2D sphereTexture)
        {
            _sphereTexture = sphereTexture;
        }

        public void DrawSpheres(IEnumerable<Sphere> spheres, SpriteBatch spriteBatch)
        {
            foreach (var sphere in spheres)
            {
                int xCenter = (int)(sphere.Position.X - _sphereSize / 2);
                int yCenter = (int)(sphere.Position.Y - _sphereSize / 2);
                Rectangle destination = new Rectangle(xCenter, yCenter, _sphereSize, _sphereSize);
                spriteBatch.Draw(_sphereTexture, destination, Color.White);
            }
        }

        public void DrawSphere(Sphere sphere, SpriteBatch spriteBatch)
        {
            int xCenter = (int)(sphere.Position.X - _sphereSize / 2);
            int yCenter = (int)(sphere.Position.Y - _sphereSize / 2);
            Rectangle destination = new Rectangle(xCenter, yCenter, _sphereSize, _sphereSize);
            spriteBatch.Draw(_sphereTexture, destination, Color.White);
        }
    }
}
